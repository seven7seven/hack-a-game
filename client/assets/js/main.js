$(document).ready(function() {

	// Draw a ball!
	$("#canvas").drawArc({
		fillStyle: "red",
		x: 50, y: 50,
		radius: 25
	});

	// Connect handler
	$('#connect').click(function(){
		init($('#host').val(),$('#username').val());
	});
});

function init(host,username) {
	// Test websockets
	var server = 'ws://' + host + ':777';
	var connection = new WebSocket(server);

	console.log('Initialized!');

	// When the connection is open, send some data to the server
	connection.onopen = function () {
		console.log('Connected!');
		connection.send("Hello from " + username);
	};

	// Log errors
	connection.onerror = function (error) {
	  console.log('WebSocket Error ' + error);
	};

	// Log messages from the server
	connection.onmessage = function (e) {
		console.log('Server: ' + e.data);
	};
}