from twisted.internet import reactor
from autobahn.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS
  
class EchoServerProtocol(WebSocketServerProtocol):
 
	def onMessage(self, msg, binary):
		print("Got message: " + msg)
		self.sendMessage(msg, binary)
 
 
if __name__ == '__main__':

	server = "ws://localhost:777"
	factory = WebSocketServerFactory(server)

	print("Server running at " + server);

	factory.protocol = EchoServerProtocol
	listenWS(factory)
	reactor.run()