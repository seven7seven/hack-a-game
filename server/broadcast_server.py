import sys
from twisted.internet import reactor
from twisted.python import log
from autobahn.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS
 
class BroadcastServerProtocol(WebSocketServerProtocol):
 
   def onOpen(self):
      self.factory.register(self)
      print("test")
 
   def onMessage(self, msg, binary):
      if not binary or true:
         message = "* Received message '%s' from %s" % (msg, self.peerstr);
         self.factory.broadcast(message)
         print(message);
 
   def connectionLost(self, reason):
      WebSocketServerProtocol.connectionLost(self, reason)
      self.factory.unregister(self)
 
 
class BroadcastServerFactory(WebSocketServerFactory):

   def __init__(self, url):
      WebSocketServerFactory.__init__(self, url)

      # Initialize client list
      self.clients = []

      # Start ticking
      self.tickcount = 0
      self.tick()
 
   def tick(self):
      self.tickcount += 1

      ## Tick toci
      self.broadcast("tick %d, %d clients connected" % (self.tickcount, len(self.clients)))

      reactor.callLater(1, self.tick)
 
   def register(self, client):
      if not client in self.clients:
         print "* Registered client " + client.peerstr
         self.clients.append(client)
 
   def unregister(self, client):
      if client in self.clients:
         print "* Unregistered client " + client.peerstr
         self.clients.remove(client)
 
   def broadcast(self, msg):
      print "* Broadcasting message '%s' .." % msg
      for c in self.clients:
         print "  send to " + c.peerstr
         c.sendMessage(msg)
 
 
if __name__ == '__main__':
 
   log.startLogging(sys.stdout)
   
   server = "ws://localhost:777"
   
   factory = BroadcastServerFactory(server)
   factory.protocol = BroadcastServerProtocol
   
   print("* Server running at " + server);
   listenWS(factory)
   reactor.run()